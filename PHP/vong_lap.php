<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body> 
  <!-- vòng lặp trong php -->
<?php
 //Vòng lặp for 
  $a = 0;
  $b = 0;
  for( $i=0;$i<5; $i++)
  {
      $a+=10;
      $b+=5;
  }
  echo("<p><font color='blue' size='25px'>sau vòng lặp, giá trị a=$a và b=$b <br></font></p>");

// vòng lặp while
$i = 0;
$num = 3;

while($i<30)
{
    $num--;
    $i++;
}
echo("<p><font color='red' size='25px'>vòng lặp dừng lại giá trị i=$i và num=$num <br></font></p>");
// vòng lặp do while
$i = 0;
do{
    $i++;
}
while($i <10);
echo("<p><font color='green' size='25px'> i=$i <br></font></p>");
// vòng lặp foreach
$array = array(1,2,3,4,5);

foreach($array as $value){
    echo "<p><font color=black size='25px'>$value<br/></font></p>" ;
}
?>
</body>
</html>