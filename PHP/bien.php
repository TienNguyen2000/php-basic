<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
   <!-- Các kiểu biến -->
    <?php  
       // kiểu số nguyên trong php
       $bien_int = 1234567890;
       echo("$bien_int  <br> ");
       // kiểu số thực trong php
       $bien_double_1 = 3.14;
       $bien_double_2 = 1.2;
       $kq = $bien_double_1 - $bien_double_2;
       print("$kq <br>");
       // kiểu boolean trong php
       if (true)
          print("văn bản được in ra.<br>");
        else 
          print("văn bản không được in ra.<br>");   
        // kiểu null trong php
        $my_var = null;
        // kiểu string trong php
        $string_1 = "ví dụ 1 <br>";
        $stirng_2 = "ví dụ 2 <br>";
        $string_3 = ""; // ví dụ một chuỗi không có kí tự
        print("$string_1 <br> ");
        // 
    ?>
</body>
</html>