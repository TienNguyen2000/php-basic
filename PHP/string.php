<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
     <!-- string trong php -->
     <!-- chuỗi là các dãy ký tự  
        * ví dụ chuỗi hợp lệ 
        $string_1 ="ví dụ chuỗi trong php";
        $string_2 =""; ví dụ chuỗi không có kí tự
    -->
    <?php  
        $string1="Tiền";
        $string2="Nguyễn";
        echo ($string1 . " " . $string2);
    // tìm độ dài chuỗi 
    $string1="Tiền";
    $string2="Nguyễn";
    echo strlen ($string1 . " " . $string2);

    ?>
</body>
</html>