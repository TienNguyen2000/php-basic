<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
     <!-- hằng (constant) -->
    <?php 
          define("Fullstack", 100);
          echo Fullstack ;
          echo constant("Fullstack");  

    // hằng hợp lệ
     define("tien", "ví dụ hợp lệ 1");
     define("tien1", "ví dụ hợp lệ 2");
     define("tien_1","ví dụ hợp lệ 3");

     // hằng không hợp lệ 
     define("2ten", "ví dụ 1");
     define("_ten_", "ví dụ 2");
     
     
    ?>
</body>
</html>